
var dsnv = [];
const DSNV = "DSNV"
var dataJson = localStorage.getItem(DSNV)
if(dataJson){
    
   var dataRaw = JSON.parse(dataJson)
   var result =[];
   for(var i = 0; i<dataRaw.length;i++){
    var currentData = dataRaw[i];
    var nv = new nhanVien(
        currentData.tk,
        currentData.name,
        currentData.email,
        currentData.pass,
        currentData.date,
        currentData.salary,
        currentData.position, 
        currentData.time,
    )
        result.push(nv)
       
   } 
   dsnv = result
    renderDsnv(dsnv)
}
function themNv(){
var newNv = layThongTinTuForm()
  var isValid = true;

  isValid= isValid & kiemTraRong(newNv.tk,'tbTKNV')&&kiemTraSo(newNv.tk,'tbTKNV')&& kiemTraTrung(newNv.tk,dsnv,'tbTKNV') 
  isValid= isValid & validate(newNv)

    if(isValid){
    dsnv.push(newNv)
   ;
    

    renderDsnv(dsnv)
    saveLocalStorage()
    reset()
    }
    
}

function saveLocalStorage(){
    var dsnvJson = JSON.stringify(dsnv);
    localStorage.setItem(DSNV,dsnvJson);
}

function xoaNv(idNv){
    var index = dsnv.findIndex(function(nv){
        return nv.tk == idNv
    })
    if(index ==-1) return
   
    
    dsnv.splice(index,1)
    console.log('dsnv: ', dsnv.length);
    renderDsnv(dsnv)
    saveLocalStorage()
    
}

function suaNv(idNv){
    var index = dsnv.findIndex(function(nv){
        return nv.tk == idNv
    })
    if(index ==-1) return
    console.log('index: ', index);
    document.getElementById('tknv').disabled = true;
    var nv = dsnv[index]
   autoClick()
    showThongTinLenForm(nv)
}

function capNhat(){
    var nvEdit = layThongTinTuForm()
    var index = dsnv.findIndex(function(nv){
        return nv.tk == nvEdit.tk
    })
    if(index ==-1) return
    console.log('index: ', index);
  

var isValid= true;
isValid = isValid & validate(nvEdit)

    if(isValid){
  dsnv[index] = nvEdit
    console.log('dsnv: ', dsnv);
    
    renderDsnv(dsnv)
    saveLocalStorage()
  
    document.getElementById('tknv').disabled = false;
    reset()
    }

}