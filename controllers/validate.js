function kiemTraRong(value,idError){
    if(value.length==0){
        document.getElementById(`${idError}`).style.display = "block"
        document.getElementById(`${idError}`).innerText = 'This field can not leave empty'
        return false
    }else{
        document.getElementById(`${idError}`).innerText = ""
        return true
    }
    }

    function kiemTraSo(value, idError){
        let isnum = /^\d+$/.test(value);
        if(!isnum){
            document.getElementById(`${idError}`).style.display = "block"
            document.getElementById(`${idError}`).innerText = 'This field only accepts numbers'
            return false   
        }else{
            document.getElementById(`${idError}`).innerText = ""
            return true  
        }
    }

    function kiemTraEmail(value, idError){
        const re = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
        var isEmail = re.test(value)
        if(!isEmail){
            document.getElementById(`${idError}`).style.display = "block"
            document.getElementById(`${idError}`).innerText = 'Invalid email. Please re-enter'
            return false   
        }else{
            document.getElementById(`${idError}`).innerText = ""
            return true  
        }
    }

    function kiemTraTrung(value,list,idError){
        var index = list.findIndex(function(nv){
            return nv.tk == value
        })
        if(index==-1){
            document.getElementById(`${idError}`).innerText = ""
            return true    
        }else{
            document.getElementById(`${idError}`).style.display = "block"
            document.getElementById(`${idError}`).innerText = 'Duplicate ID was found. Please add another account'
            return false  
        }
    
    }
    function kiemTraChu(value,idError){
        const re =/^[a-zA-Z]+ [a-zA-Z]+$/
        var isString = re.test(value)
        if(!isString){
            document.getElementById(`${idError}`).style.display = "block"
            document.getElementById(`${idError}`).innerText ='Only alphanumeric characters are allowed for this field'
            console.log('value: ', value);
            console.log('typeof: ', typeof(value));
            return false
        }  
// it's a string
else{
    document.getElementById(`${idError}`).innerText = ""
            return true  
}
// it's something else
    }

    function kiemTraPassword(value,idError){
        const re =  /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,10}$/
        var isPassword = re.test(value)
        if(!isPassword){
            document.getElementById(`${idError}`).style.display = "block"
            document.getElementById(`${idError}`).innerText = 'Password contains at least 1 numeric character, 1 uppercase character, 1 special character and has a minimum length of 6 to 10 characters'
            console.log('password: ', value);
            return false   
        }else{
            document.getElementById(`${idError}`).innerText = ""
            console.log('password: ', value);
            return true  

        }
    }

    function kiemTraNgay(value,idError){
        const re = /^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/;
        var isValidDate = re.test(value);
        if(!isValidDate){
            document.getElementById(`${idError}`).style.display = "block"
            document.getElementById(`${idError}`).innerText = 'Please enter the date in mm/dd/yyyy format'
            console.log('value: ', value);
        
            return false   
        }else{
            document.getElementById(`${idError}`).innerText = ""
          

            return true
        }
    }

    function kiemTraGioLam(value,idError){
        
        if(value>=80&&value<=200){
            document.getElementById(`${idError}`).innerText= ""
            console.log('value: ', value);

            return true
        }else{
            document.getElementById(`${idError}`).style.display = "block"
            document.getElementById(`${idError}`).innerText = 'Please enter the number of hours worked in 1 month (from 80 to 200 hours)'
            console.log('value: ', value);
            return false
        }
    }

    function kiemTraLuong(value,idError){
        if(value>=1000000&&value<=20000000){
            document.getElementById(`${idError}`).innerText = ""
            console.log('value: ', value);
           return true
        }else{
            document.getElementById(`${idError}`).style.display = "block"
            document.getElementById(`${idError}`).innerText = 'Please enter a specific salary from 1000000 to 20000000'
            console.log('value: ', value);  
            return false
        }
    }

    function kiemTraChucVu(value,idError){
      var option =  document.getElementById('chucvu')
      if(option.value == "trong" || option.value == ""){
        document.getElementById(`${idError}`).style.display = "block"
        document.getElementById(`${idError}`).innerText = 'Please enter specific job title'
        console.log('value: ', value);
        return false
      }else{
        document.getElementById(`${idError}`).innerText= ""
        console.log('value: ', value);

        return true
      }
    }
    

    