
function layThongTinTuForm(){
    var tk = document.getElementById('tknv').value.trim();
    var name = document.getElementById('name').value.trim();
    var email = document.getElementById('email').value.trim();
    var pass = document.getElementById('password').value.trim();
    var date = document.getElementById('datepicker').value.trim();
    var salary = document.getElementById('luongCB').value.trim();
    var position = document.getElementById('chucvu').value.trim();
    var time = document.getElementById('gioLam').value.trim();

    var nv = new nhanVien(tk,name,email,pass,date,salary,position,time)
    return nv
}
function renderDsnv(list){
   
    var contentHTML =""
    for(var i =0; i<list.length; i++){
        var currentNv = list[i]
        var tr = `
        <tr>
        <td>${currentNv.tk}</td>
        <td>${currentNv.name}</td>
        <td>${currentNv.email}</td>
        <td>${currentNv.date}</td>
        <td>${currentNv.position}</td>
        <td>${currentNv.calculation()}</td>
        <td>${currentNv.xepLoai()}</td>
        <td>
        <button onclick="xoaNv('${currentNv.tk}')" class="btn btn-danger">Remove</button>
        
        
        <button onclick="suaNv('${currentNv.tk}')" class="btn btn-success">Edit</button>
        </td>
        
        
        </tr>`
        contentHTML += tr
}
return document.getElementById('tableDanhSach').innerHTML = contentHTML;
}

function showThongTinLenForm(nv){
document.getElementById('tknv').value = nv.tk
document.getElementById('name').value = nv.name
document.getElementById('email').value = nv.email
document.getElementById('password').value = nv.pass
document.getElementById('datepicker').value = nv.date
document.getElementById('luongCB').value = nv.salary
document.getElementById('chucvu').value = nv.position
document.getElementById('gioLam').value = nv.time
}

function autoClick(){
    document.getElementById('btnThem').click();
}

function reset(){
    document.getElementById('formQLNV').reset()

}

function validate(nv){
    
    var isValid = true;
    
    isValid &= isValid & kiemTraRong(nv.name,'tbTen') &&kiemTraChu(nv.name,'tbTen')

    isValid &= isValid & kiemTraRong(nv.email, 'tbEmail')&& kiemTraEmail(nv.email, 'tbEmail')
    
    isValid &= isValid & kiemTraRong(nv.pass, 'tbMatKhau') && kiemTraPassword(nv.pass, 'tbMatKhau')
   
    isValid &= isValid & kiemTraRong(nv.date, 'tbNgay') &&kiemTraNgay(nv.date, 'tbNgay')
    
    isValid &= isValid & kiemTraRong(nv.salary, 'tbLuongCB') &&kiemTraLuong(nv.salary, 'tbLuongCB')
    isValid &= isValid & kiemTraChucVu(nv.position,'tbChucVu')
   
    isValid &= isValid & kiemTraRong(nv.time, 'tbGiolam') &&kiemTraGioLam(nv.time, 'tbGiolam')

    return isValid
}

function search(){
    var input, filter, table, tr, td, i, txtValue;
    input = document.getElementById('searchName');
    filter = input.value.toUpperCase();
    table = document.getElementById('tableDanhSach');
    tr = table.getElementsByTagName('tr');
    for(i=0; i<tr.length; i++){
        td = tr[i].getElementsByTagName('td')[6];
        if(td){
            txtValue = td.textContent || td.innerText;
            if(txtValue.toUpperCase().indexOf(filter)>-1){
                tr[i].style.display = ""
            }else{
                tr[i].style.display = "none"
            }
        }
    }
}


// function kiemTraTrung(value,list,idError){

// }
